import React from "react";
import { Colour } from './Colour';

interface ColourListProps {
    onClick(): () => void;
    colour:Colour;
}
export class ColourListItem extends React.Component<ColourListProps> {
    render() {
        return (
            <div onClick={this.props.onClick()}>
                <code>{this.props.colour.name}</code>
            </div>
        )
    }
}