export class Colour {
    public constructor(public readonly red: number, public readonly green: number, public readonly blue: number, public readonly name: string) { }
    public static newColour(red: number, green: number, blue: number): Colour {
        if (red < 0 || 255 < red) {
            alert("Value of red cannot be " + red);
            return new Colour(127, 127, 127, "incorrect");
        } else if (green < 0 || 255 < green) {
            alert("Value of green cannot be " + green);
            return new Colour(127, 127, 127, "incorrect");
        } else if (blue < 0 || 255 < blue) {
            alert("Value of blue cannot be " + blue);
            return new Colour(127, 127, 127, "incorrect");
        }
        const hex: string = "#" + Colour.toHex(red) + Colour.toHex(green) + Colour.toHex(blue);
        return new Colour(red, green, blue, hex);
    }
    asString(): string {
        if (this.red === undefined || this.blue === undefined || this.green === undefined) {
            alert("Warning! Color  \"" + this.name + "\" doesn't exists"); //"Warning! Color undefined doesn't exists"
            return "";
        }
        return "rgb(" + this.red + "," + this.green + "," + this.blue + ")";
    }
    public static toHex(data: number) {
        return (15 < data) ? data.toString(16) : "0" + data.toString(16);
    }
    isEqual(otherColour: Colour): boolean {
        return (otherColour.red === this.red && otherColour.green === this.green && otherColour.blue === this.blue);
    }
}


