import React from "react";
import {SAFE_COLOURS} from "./SafeColour";
import {Colour} from './Colour';
import {ColourListItem} from "./ColourListItem";
import {renderArray} from '../arrayDrawer';
import {getRandomArray} from "../dataGenerator";
import {AddColour} from "./AddColour";


interface ColourPicerState {
    selectedColours: Colour[];
}

interface ColourPickerProps {
    length: number;
}

export class ColourPicker extends React.Component<ColourPickerProps, ColourPicerState> {
    constructor(props: ColourPickerProps) {
        super(props);
        this.state = {
            selectedColours: SAFE_COLOURS.concat(),
        }

    };

    deleteColour(colourToDelete: Colour): void {
        if (this.state.selectedColours.length <= 3) return;
        this.setState({
            selectedColours: this.state.selectedColours.filter((value) => {
                return value !== colourToDelete
            }),
        });
    }


    addColour(newColour: Colour) {
        if (!this.state.selectedColours.some((color) => color.isEqual(newColour))) {
            this.setState({
                selectedColours: this.state.selectedColours.concat([newColour])
            });
        }

    }

    render() {
        let selectedColours = this.state.selectedColours.map((colour: Colour, index: number) => {
            return (
                <ColourListItem key={index} colour={colour} onClick={() => (() => this.deleteColour(colour))}/>
            )
        });
        return (
            <>
                <AddColour onChange={(colourToAdd) => this.addColour(colourToAdd)}/>

                <form name="colour-picker">
                    {renderArray(20, this.props.length, getRandomArray(this.props.length, this.state.selectedColours))}
                    {selectedColours}
                </form>
            </>

        )
    }

}
