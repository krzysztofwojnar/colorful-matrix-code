import React from "react";
import { Colour } from './Colour/Colour';

export function renderArray(singleSquareSize: number, sideLengthOfTheCode: number, arrangement: Colour[]): JSX.Element {
    if(singleSquareSize < 0 || sideLengthOfTheCode < 1 || arrangement == null) return <p>Incorrect paramteres</p>;
    let rows: Colour[][] = [];
    for (let position = 0; position <= arrangement.length - 1; position += sideLengthOfTheCode) {
        rows.push(arrangement.slice(position, position + sideLengthOfTheCode));
    }
    return (<svg width={singleSquareSize * sideLengthOfTheCode} height={singleSquareSize * sideLengthOfTheCode}>
        {rows.map((row, index) => generateRow(singleSquareSize, index, row))}
    </svg>)
}

function generateRow(singleSquareSize: number, topOffset: number, row: Colour[]): JSX.Element[] {
    return row.map((colour: Colour, index) => {
        return <rect
            x={index * singleSquareSize}
            y={topOffset * singleSquareSize}
            width={singleSquareSize}
            height={singleSquareSize}
            fill={colour.asString()}
            key={row.length * topOffset + index} />
    })
}